                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2802815
Anet A8 slim and robust x-axis tensioner by juh is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This x-belt tensioner **combines several advantages** of other designs:

1. moves force of tension from z-axis to x-axis rods to prevent z-axis bending (similar to many others e.g. [this](https://www.thingiverse.com/thing:1683070) or [this](https://www.thingiverse.com/thing:1921667))
2. allows to reuse existing belt and pulley by placing pulley to the left of the x-axis mount (e.g. like [this](https://www.thingiverse.com/thing:1999669))
3. doesn't increase the footprint of the printer, i.e. no parts will stick out to the right (similar to [this](https://www.thingiverse.com/thing:2196516)
4. is robust (like [this](https://www.thingiverse.com/thing:2461317))
5. doesn't cut into  x-axis width (other than e.g. [this](https://www.thingiverse.com/thing:1999669))
6. overall minimalist design, keeping things as simple as possible, but not simpler

I generally liked [lokster](https://www.thingiverse.com/lokster/about)'s [solution](https://www.thingiverse.com/thing:2196516) due to compliance with design goal 3, but as [some reported problems](https://www.thingiverse.com/thing:2196516/) (comment 1402077 by mcorl)  regarding the strength of the cap part, and [this](https://www.thingiverse.com/thing:2461317) improvement conflicted with design goal 3, I moved the reinforcing parts to go inside the right hand z-axis mount. Accordingly I shortened the pulley part, which gave me the opportunity to also make it a bit lighter.

All my Anet A8 designs:
* [Anet A8 lightweight and unobtrusive X-axis cable chain](https://www.thingiverse.com/thing:2785408)
* [Anet A8 slim and robust x-axis tensioner](https://www.thingiverse.com/thing:2802815)
* [Anet A8 power supply cover with LCD power meter and switch](https://www.thingiverse.com/thing:2815307)


# Print Settings

Printer: Anet A8
Rafts: Doesn't Matter
Supports: Yes
Resolution: .2
Infill: 75% or more

# Post-Printing

## Installation

You'll need

* 1 screw 3mm x 18-20mm + nut for the pulley
* 1 screw 3mm x 30-40mm + nut for the tensioner