                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2442826
Anet A8 Bowden Flex Update by AlexWaringer is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

This is a mounting block improvement for the MK8 Extruder used for my Anet A8 with bowden setup and the extruder mounted on the frame with the mount: 
https://www.thingiverse.com/thing:2146021

The profiles of the block lead to a minimum distance to the drive gear and the counter roll that the flex filament will not bend away.

A PTFE tube must be inserted in the bore hole in the profile. The tube must be cutted to the profile.
The pneumatic fitting of the bowden can be screwed into the plastic as well as the thread rod with the nut for the spring guide.

For individual changes I have attached the part as parasolid.

# Print Settings

Printer: Anet A8
Rafts: Doesn't Matter
Supports: No
Resolution: 0.12
Infill: 30%

Notes: 
I recommend PETG or ABS.