$fn=30;

intersection()
{
    translate([65,20,8])
    rotate([-90,0,0])
    rotate([0,90,0])
        import("A8-0004.stl");

    cube([100,100,8]);
}

translate([40,37,0])
cube([10,25,8]);

translate([15,30,8])
{
    difference()
    {
        union()
        {
            difference()
            {
                union()
                {
                    difference()
                    {
                        cube([25,25,150]);
                        translate([3.5,4,0])
                            cube([18,21,150]);
                    }
                    translate([10.5,4,0])
                        cube([4,21,150]);
                    translate([0,0,128])
                        cube([25,25,4]);
                }
                translate([12.5,0,130])
                rotate([-90,0,0])
                    cylinder(d=8.4, h=25);
            }

            translate([12.5,3,130])
            rotate([-90,0,0])
            difference()
            {
                cylinder(d=16.4, h=22);
                cylinder(d=8.4, h=22);
            }
        }
        translate([8.3,0,121.5])
            cube([8.4,6,8]);
    }
}

