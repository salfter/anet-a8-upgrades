RAMPS 60mm Fan Mount
====================

Remixed for a single 60mm fan instead of a pair of 40mm fans.  The mount I
used previously was screwed to the board; this one clips to the edge, making
service and adjustments easier.
