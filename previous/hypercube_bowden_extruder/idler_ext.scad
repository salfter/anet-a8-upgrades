ext=1.0;

module orig()
{
    translate([-54,-55,0])
        import("Bowden_Idler.stl");
}

// hinge area
intersection()
{
    orig();
    cube([20,30,10]);
}

// bottom of idler area
intersection()
{
    orig();
    translate([0,-40,0])
        cube([20,40,5]);
}

// new section to extend
intersection()
{
    orig();
    translate([0,-40,5])
        cube([20,40,ext]);
}

// top of idler area
translate([0,0,ext])
intersection()
{
    orig();
    translate([0,-40,5])
        cube([20,40,15]);
}
