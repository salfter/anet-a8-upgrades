mockup();   
new_duct();

module mockup()
{
    translate([-31,21,13.5])
    {
        rotate([90,0,0])
            %import("../e3d-v6-carriage/E3DV6_BasePlate_V4_fixed.stl");
        translate([69,-24,26])
        rotate([0,180,0])
            %import("../alternate-bltouch-mount-2/mount.stl");
        translate([51.5,-56,35.5])
        rotate([0,0,180])
            %import("../e3d-v6-carriage/E3DV6_E3DHolder_with_Chain_No_Blower_chain_mod.stl");
    }

    translate([3.4,5.1,43.3])
    rotate([0,0,90])
        %import("E3D_v6_To_Spec.stl");
}

module old_duct()
{
    translate([-5,-35,25])
    rotate([90,0,0])
    color([.2,.2,.2,.5])
        %import("50mm_Fan.stl");

    translate([-3.9,-49.2,24.9])
    translate([113.9,134.7,-22.9])
    rotate([0,0,180])
        %import("../fan_duct/Shorter_Semi-Circular_Anet_A8_Fan_Duct.stl");
}

// section to be inserted into blower: 16.6x13.1x7

module new_duct()
{
    // mounting tab

    difference()
    {
        union()
        {
            translate([-31,12,9.5])
                cube([15,4,26]);
            translate([-31,10,9.5])
                cube([15,4,6]);
        }
        translate([-26,0,20.5])
        rotate([-90,0,0])
            cylinder(d=3.4, $fn=20, h=30);
        translate([-21,0,31])
        rotate([-90,0,0])
            cylinder(d=3.4, $fn=20, h=30);
    }

    // duct body

    translate([-31,10,1.5])
    difference()
    {
        union()
        {
            cube([46,6,8]);
            translate([31,-10,-1.5])
            {
                nozzle();
                mirror([1,0,0])
                    nozzle();
            }
        }
        translate([.8,.8,.8])
            cube([44.4,4.4,6.4]);
        translate([31,-10,-1.5])
        {
            nozzle_bore();
            mirror([1,0,0])
                nozzle_bore();
        }

        translate([.8,0,.8])
            cube([7,.8,3.5]);
    }

    translate([14.2,10,1.5])
        cube([0.8,6,6]);

    // duct inlet

    translate([-22,-10,12])
    rotate([0,0,90])
    {
        translate([18,-1,0])
        rotate([90,0,180])
        color([.2,.2,.2,.5])
            %import("50mm_Fan.stl");

        difference()
        {
            cube([16.6,13.1,7]);
            translate([1.2,1.2,0])
                cube([14.2,10.7,7]);
        }
    }

    translate([-22,-10,1.5])
    rotate([0,-90,0])
    linear_extrude(1.2)
        polygon([[0,0],[10.5,0],[10.5,20.1],[0,20.1],[0,6.05],[6.05,0]]);
    translate([-33.9,-10,1.5])
    rotate([0,-90,0])
    linear_extrude(1.2)
        polygon([[0,0],[10.5,0],[10.5,20.1],[0,20.1],[0,6.05],[6.05,0]]);

    translate([-35.1,-3.95,1.5])
        cube([13.1,14,1.2]);

    translate([-35.1,-10,7.55])
    rotate([-45,0,0])
        cube([13.1,8.56,1.2]);

    translate([-35.1,-10,7.55])
        cube([13.1,1.2,4.5]);

    translate([-35.1,8.1,8])
        cube([5.05,2,9.5]);
    translate([-27.05,8.1,8])
        cube([5.05,2,9.5]);

    translate([-35.1,10,1.5])
        cube([4.1,1.2,14.05]);
}

module nozzle()
{
    difference()
    {
        rotate([0,-17,45])
        translate([6,0,0])
        rotate([0,90,0])
        difference()
        {
            cylinder(d1=5.6, d2=7.1, h=13, $fn=60);
            cylinder(d1=4.0, d2=5.5, h=13, $fn=60);
        }
        translate([15,10,0])
            cube([10,10,10]);
        translate([0,0,-10])
            cube([20,20,11.5]);
    }
}

module nozzle_bore()
{
    rotate([0,-17,45])
    translate([6,0,0])
    rotate([0,90,0])
        cylinder(d1=4.0, d2=5.5, h=13, $fn=60);
}