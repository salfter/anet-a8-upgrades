                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2904778
RAMPS/Raspberry Pi/Dual MOSFET Mount for Anet A8 by salfter is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

The original motherboard for my A8 has been getting flaky lately, so I replaced it.  I needed something to hold it in place and the existing options didn't quite do what I wanted, so I knocked this together in OpenSCAD.  It holds an Arduino Mega (with a RAMPS stacked on top), a Raspberry Pi Model B+ or later, and a couple of MOSFET boards (which I had been using with the original motherboard).  It bolts up where the original motherboard went, so you don't need to drill any new holes.  If you don't like the placement of components (maybe you're not using external MOSFETs, for instance), it's easy to move them around to get the configuration you want.

This design has gone through a few iterations to get it to where I can bolt it up and put it to use.  Still have to tidy up the cables, but everything's up and running again.

**Update:** (6 Jun 19) Upgrading to [a 200W bed heater](https://gulfcoast-robotics.com/collections/heated-beds/products/aluminum-heated-bed-build-plate-for-v2-reprap-3d-printer-12v-200w-with-heater-full-kit) required upgrading to [a bigger MOSFET board](https://amzn.to/2Wn0LV1).  A new configuration to support this board has been added.

**Update:** (7 Jun 19) Mounting points for the [Re-ARM](https://www.panucatt.com/Re_ARM_for_RAMPS_p/ra1768.htm) 32-bit upgrade have been added.

# Print Settings

Printer Brand: Anet
Printer: A8
Rafts: No
Supports: No
Resolution: 0.2 mm
Infill: 25%
Filament_brand: Dazzle Light
Filament_color: purple
Filament_material: PLA

Notes: 
The printed holes should be usable as-is.  I made the board-mounting holes slightly larger than the frame-mounting holes because the board-mounting holes were so tight that the standoffs would break right off.