// measured dimensions: 86.40 x 38.20 mm

width=86.40;
height=38.20;

hole_spacing=85.50;

slack=0.2;
thickness=4.0;
bracket_width=15;

difference()
{
    union()
    {
        difference()
        {
            cube([width+slack+2*thickness, height+slack+2*thickness, bracket_width]);
            translate([thickness, thickness, 0])
                cube([width+slack, height+slack, bracket_width]);
            translate([(width+slack+2*thickness)/2-1, 0, 0])
                cube([2, thickness, bracket_width]);
        }
        translate([-7,height+slack+thickness,0])
            cube([7,thickness,bracket_width]);
    }

    translate([-3.5,height+slack+thickness,bracket_width/2])
    rotate([-90,0,0])
        cylinder(d=6.8, $fn=45, h=thickness-2);
    translate([-3.5,height+slack+thickness,bracket_width/2])
    rotate([-90,0,0])
        cylinder(d=3.4, $fn=30, h=thickness);

    translate([hole_spacing-3.5,height+slack+thickness,bracket_width/2])
    rotate([-90,0,0])
        cylinder(d=6.8, $fn=45, h=thickness-2);
    translate([hole_spacing-3.5,height+slack+thickness,bracket_width/2])
    rotate([-90,0,0])
        cylinder(d=3.4, $fn=30, h=thickness);
}

translate([(width+slack+2*thickness)/2-16, 0, 0])
difference()
{
    linear_extrude(bracket_width)
        polygon([[0,0],[15,0],[15,-10],[10,-10]]);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=6.8, $fn=6, h=12);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=3.4, $fn=30, h=15);
}

translate([(width+slack+2*thickness)/2+16,0,bracket_width])
rotate([0,180,0])
difference()
{
    linear_extrude(bracket_width)
        polygon([[0,0],[15,0],[15,-10],[10,-10]]);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=6.8, $fn=45, h=12);
    translate([0,-5,bracket_width/2])
    rotate([0,90,0])
        cylinder(d=3.4, $fn=30, h=15);
}
