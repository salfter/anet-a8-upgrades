function pi()=3.14159265358979323846;
function int(value)=round(value+0.5)-1;



$fn=80;
waveSize=2;
waveHeight=42;
waveDiameter=39;
//waveTeethAmount=10;
realDiameter=waveDiameter-waveSize;
teethAmount=int((realDiameter*pi())/waveSize);
//teethAmount=36;

echo(360/teethAmount);
difference(){
union(){
translate([0,0,8])cube([41.5,41.5,16],center=true);

}
union(){

difference(){
union(){

//translate([0,0,waveHeight])cylinder(d=12,h=0.4);
//translate([0,0,waveHeight])cylinder(d=8,h=7);
//translate([0,0,waveHeight])cylinder(d=5,h=23);
//for(i=[1:(360/teethAmount)*2:360]){
//echo(sin(i));
//echo(sin(i)*realDiameter);
//translate([sin(i+90)*realDiameter/2,sin(i)*realDiameter/2,9])
//cylinder(d=waveSize+0.5,h=waveHeight);
//}
}
}
//#translate([0,0,-2])cylinder(d=22,h=2);
}
translate([0,0,0])cylinder(d=22,h=2);
translate([-12.75,0,4.25])cube([16,41.5,8.5],center=true);
translate([0,14.85,8])cube([41.5,11.8,16],center=true);
translate([0,-14.2,8])cube([41.5,13.1,16],center=true);
#translate([-15.25,0,8])cube([13,41.5,16],center=true);


#rotate([90,0,0])translate([-7,10.5,-25])cylinder(d1=2.2,d2=2.4,h=50);
#rotate([90,0,0])translate([-7,10.5,2])cylinder(d1=3.2,d2=2.3,h=4);
#rotate([90,0,0])translate([-7,10.5,-9.5])cylinder(d1=3.2,d2=2.2,h=2);
rotate([90,0,0])translate([11,10.5,-25])cylinder(d=9,h=50);
translate([0,0,0])cylinder(d=12,h=24);
translate([-14,0,0])cylinder(d=14,h=24);
translate([-12,1,0])cylinder(d=14,h=24);
translate([15.5,15.5,0])cylinder(d=3.3,h=24);
translate([-15.5,15.5,0])cylinder(d=3.3,h=24);
translate([15.5,-15.5,0])cylinder(d=3.3,h=24);
translate([-15.5,-15.5,0])cylinder(d=3.3,h=24);
//translate([15.5,15.5,0])cylinder(d=5.6,h=14);
//translate([-15.5,15.5,0])cylinder(d=5.6,h=14);
//translate([15.5,-15.5,0])cylinder(d=5.6,h=14);
//translate([-15.5,-15.5,0])cylinder(d=5.6,h=14);

//translate([15.5,15.5,16])cylinder(d1=5.5,d2=7,h=2);
//translate([-15.5,15.5,16])cylinder(d1=5.5,d2=7,h=2);
//translate([15.5,-15.5,16])cylinder(d1=5.5,d2=7,h=2);
//translate([-15.5,-15.5,16])cylinder(d1=5.5,d2=7,h=2);


translate([17.3,21.40,0])rotate([0,0,-45])cube([6,2,24]);
translate([-21.4,17.30,0])rotate([0,0,45])cube([6,2,24]);
translate([18.6,-22.90,0])rotate([0,0,45])cube([6,2,24]);
translate([-22.9,-18.60,0])rotate([0,0,-45])cube([6,2,24]);
//#translate([0,0,5])rotate([0,90,0])cylinder(d=4,h=24);

}
