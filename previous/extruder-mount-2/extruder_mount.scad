$fn=45;

module spacer(t)
{
    linear_extrude(t) // set thickness here
    difference()
    {
        minkowski()
        {
            square(31.3, center=true);
            circle(d=11);
        }
        translate([-15.5,-15.5])
            circle(d=3.4);
        translate([15.5,-15.5])
            circle(d=3.4);
        translate([-15.5,15.5])
            circle(d=3.4);
        translate([15.5,15.5])
            circle(d=3.4);
        circle(d=23);
    }
}

rotate([180,0,0])
{
    translate([24,16,-12.72])
    rotate([0,-90,180])
    difference()
    {
        translate([89.72,-20.12,14.25])
            import("dovetail-mount/dovetail_test_F_fixed.stl");

        cylinder(d=3.4, h=7.2);

        translate([0,0,4.64])
            cylinder(d=6.4, h=2.5, $fn=6);
    }

    spacer(2);

    translate([17.5,-9,0])
        cube([7.5,30,2]);
}