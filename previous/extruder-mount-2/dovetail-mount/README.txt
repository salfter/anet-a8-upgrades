                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:2137301
Anet A8 Dovetail Accessory and Spoolholder Mount by svhb is licensed under the Creative Commons - Attribution - Non-Commercial license.
http://creativecommons.org/licenses/by-nc/3.0/

# Summary

Dovetail mount spool holder with ability to mount future accessories such as a filament guide, light or camera, perhaps even a bowden extruder. The holder mounts between the printer frame and the LCD board (after removing original spacers).
Spool holder tower spacing is adjustable, so either original threaded rod or a customized rod can be used. The rod openings are closed on the outsides so that there is no need for nuts or other devices to keep the rod positioned horizontally. The end walls can be cut or broken out if a longer rod is used.
The adjustable position plus closed-end rod holders allows for the use of wide spools or large spool adapters.
Dovetail test pieces are provided to test whether the dovetails will work with your printer settings before you commit to printing the large pieces.

This design was inspired by http://www.thingiverse.com/thing:1505075 by DeAtHfOrToLd and http://www.thingiverse.com/thing:1743397 by dldesign (remix of DeAtHfOrtoLd's) but I didn't call it a remix since it doesn't actually use anything directly from either of those designs. I actually printed the one from dldesign, but found that some wide spools don't fit between the posts, and neither did my current favorite spool holder (http://www.thingiverse.com/thing:1241566) so I decided a wider one was needed.
BTW, another option I didn't see until after I made mine is this one: http://www.thingiverse.com/thing:2064776 by tzahov. It uses a very cool mounting technique.

UPDATE: I added a couple of simple filament guides. One is 50mm long, the other is 70mm.

NOTE: DO NOT try to print the "assembled" STL. It is not likely to work well, that file is included only for the purpose of showing what the full assembly looks like.

# Print Settings

Printer: Anet A8
Supports: Yes
Resolution: .25mm
Infill: 25%

Notes: 
All dovetails should be printed facing up, and in the same X/Y orientation. If you print one side horizontally on the print bed and the other vertically, the resulting fit may be affected by variations in scaling between the two axes.

Supports are needed only for the openings on the top ends of the towers (called "arms" in files)

The dovetail fit is likely to be a bit finicky. Please print the test pieces first to make sure the dovetails fit together before wasting a bunch of plastic and time on the larger pieces. If you can't find slicer settings that give working dovetails, let me know and I will try to help you. If needed I could make a second set of parts with looser fitting dovetails.



# How I Designed This

I originally used a single dovetail, but the spool holder towers were too wobbly. Using two dovetails fixed this problem. I also added some holes to attach clamping pieces to the back, but these were not needed so I didn't need to design those pieces.
The grooves across the backs of the parts are there to reduce the possibility of warping. The first tower I printed warped badly enough to be completely unusable. Grooves on the front of the towers are there for cosmetics only, they match the grooves on the back.