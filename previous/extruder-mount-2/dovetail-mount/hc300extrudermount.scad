// translate([15,46,-53.5])
// rotate([90,0,-90])
// %import("mk8_extruder_mount_fixed.stl");

difference()
{
    translate([89.7,-24.1,14.25])
    import("dovetail_test_F_fixed.stl");

    translate([15,20.9,0])
    rotate([90,0,0])
    rotate([0,-90,0])
    linear_extrude(6)
    polygon([[0,0],[0,6],[6,0]]);

    translate([0,21,6.5])
    rotate([90,0,0])
    cylinder(d=4.8, $fn=30, h=5);
}

