difference()
{
    translate([0,0,38.8])
    rotate([180,0,0])
        import("camera_holder_bracket_for_anet_a8.stl");

    translate([0,0,50])
    cube([50,60,50], center=true);
}

translate([-13.5,-31.25,6])
    cube([20,12,12], center=true);