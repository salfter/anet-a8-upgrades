                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2392400
Spriya Anet A8 Fan Duct Modified for shorter nozel height by Dazwell is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

So I've only just got my A8, and was advised that I needed to change to standard fan duct for this one. So I printed it out (and it came out perfect) but, the overall height of the duct was too high. I have my nozzle set quite high in the block, so that it's easier to insert the filament after the extruder.
Fortunately, the original came with the blender file, so it was easy for me to remove 2mm from the top, and move the outlet nozzle's up 3mm. Re-printed and it fits (and works) perfectly.
It still looks exactly the same as the original, just not as high over all.

# Print Settings

Printer: ANET A8
Rafts: Yes
Supports: Yes
Resolution: 0.2
Infill: 20%

Notes: 
As long as you rotate on the X by 90 degrees, so that you are using the front face as the base, it will come out fine. It is a bit fiddly getting the internal supports out, but once done, it's a thing of beauty :)