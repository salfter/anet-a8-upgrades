$fn=30;

translate([8,-10,20])
rotate([90,-90,180])
%import("1_LCD_BottomBody.stl");

difference()
{
    union()
    {
        translate([0,0,-20])
        cube([6,40,40]);

        translate([-2,0,7])
        cube([2,40,6]);

        translate([-2,0,-13])
        cube([2,40,6]);

        translate([-9,0,0])
        difference()
        {
            translate([15,-60,-10])
                cube([4,100,30]);
            translate([15,-35,-10])
                cube([4,50,20]);
        }
    }

    translate([-10,-44.5,7])
    rotate([0,90,0])
    {
        cylinder(d=5, h=20);
        translate([0,69,0])
            cylinder(d=5, h=50);
    }

    translate([-3,10,-10])
    rotate([0,90,0])
    {
        cylinder(d=5.5, h=20);
        translate([0,0,5])
            cylinder(d=12, h=20);
    }

    translate([-3,30,-10])
    rotate([0,90,0])
    {
        cylinder(d=5.5, h=20);
        translate([0,0,5])
            cylinder(d=12, h=20);
    }
}