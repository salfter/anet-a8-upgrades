$fn=30;

bondtech=1; // 0 for MK8

difference()
{
    translate([91.75,-138,-5.6])
        import("filamentsensor.stl");

    translate([0,15,0])
    rotate([90,0,0])
        cylinder(d=2.75, h=30); // 2.6 mm kinda works for ABS; trying 2.75 for PETG

    if (bondtech==1)
    {
        translate([0,-3,0])
        rotate([90,0,0])
            cylinder(d=4.4, h=8); 
    }
    else
    {
        translate([0,-3.2,0])
        rotate([90,0,0])
            cylinder(d2=2.5, d1=6, h=7);
    }

    translate([0,-.075,0.2]) // hole's not wide enough for sensor...need 12.5 mm
        cube([13.3,7,8.5], center=true);    
}

