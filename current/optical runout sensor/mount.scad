$fn=60;

bondtech=1; // 0 for MK8, 1 for Bondtech

print(); 

module print()
{
    rotate([-90,0,0])
        do_mount();

    if (bondtech==1)
    {
        translate([20,20,9.6])
        rotate([0,-90,0])
            do_sensor();
    }
    else
    {
        translate([-70,0,-70])
            do_sensor();
    }
}

module mockup()
{
    do_mount();
    do_sensor();
}

module do_mount()
{
    translate([0,-2,0])
        import("2020_Extruder_Motor_Mount.stl");
    translate([21.5,-20,0])
        cube([4,20,70]);
    translate([-25.5,-20,0])
        cube([4,20,70]);
    difference()
    {
        translate([-25.5,-20,60])
            cube([51,20,10]);

        translate([-4.5,-15,59])
            cylinder(d=3.4, h=40);
        translate([-4.5,-15,59])
        rotate([0,0,30])
            cylinder(d=6.5, h=8 ,$fn=6);
        translate([-4.5,-5,59])
            cylinder(d=3.4, h=40);
        translate([-4.5,-5,59])
        rotate([0,0,30])
            cylinder(d=6.5, h=8, $fn=6);
    }

    // extruder
    if (bondtech==1)
    {
        translate([21,38-5,.5])
        rotate([-90,180,0])
        %import("BMG Extruder.stl");
    }
    else
    {
        translate([0,4-4,0])
        translate([0,0,21.5])
        rotate([0,0,90])
        %import("../runout sensor mount/MK8.stl");
    }

    // filament path
    translate([5.6-9.6*bondtech,(14.1+13.4*bondtech)-4-bondtech,-10])
    %cylinder(d=1.75, h=200);
}

module do_sensor()
{
    // runout sensor
    translate([-18*bondtech,(33.15*bondtech)-4-bondtech,23*bondtech+22.98*(1-bondtech)])
    rotate([0,0,-90*bondtech])
    {
        translate([-6.75,21.5,54])
        rotate([180,0,0])
        %import("Optical endstop.stl");

        difference()
        {
            union()
            {
                if (bondtech==1)
                {
                    translate([5.7,14,57.1])
                    rotate([-90,180,0])
                        import("filamentsensor_fixed_bondtech.stl");
                }
                else
                {
                    translate([5.7,14,57.1])
                    rotate([-90,180,0])
                        import("filamentsensor_fixed.stl");
                }

                translate([-12,8.4,51])
                    cube([8.5,10,12]);

                translate([14.6,8.4,51])
                    cube([8.5,10,12]);
            }

            translate([-4,20,57])
            rotate([90,0,0])
                cylinder(d=3.1, h=8);

            translate([15,20,57])
            rotate([90,0,0])
                cylinder(d=3.1, h=8);
        }
    }

    // connector between spacer and sensor
    if (bondtech==1)
    {
        translate([-9.6,0,70])
            cube([10,11,20]);

        difference()
        {
            translate([0.4,0,70])
            rotate([90,0,-90])
            linear_extrude(10, convexity=10)
                polygon([[0,0],[20,0],[0,20]]);
            
            translate([-4.5,-15,59])
                cylinder(d=3.4, h=40);
            translate([-4.5,-15,73])
                cylinder(d=8, h=40);
            translate([-4.5,-5,59])
                cylinder(d=3.4, h=40);
            translate([-4.5,-5,73])
                cylinder(d=8, h=40);
        }
    }
    else
    {
        difference()
        {
            translate([0.4,5,70])
            rotate([90,0,-90])
            linear_extrude(10, convexity=10)
                polygon([[0,0],[25,0],[0,20]]);
            
            translate([-4.5,-15,59])
                cylinder(d=3.4, h=40);
            translate([-4.5,-15,73])
                cylinder(d=8, h=40);
            translate([-4.5,-5,59])
                cylinder(d=3.4, h=40);
            translate([-4.5,-5,73])
                cylinder(d=8, h=40);
        }
    }
}