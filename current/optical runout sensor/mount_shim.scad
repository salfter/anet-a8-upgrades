$fn=30;
t=1;

difference()
{
    cube([27,12,t], center=true);
    cube([13,6.5,t], center=true);
    translate([-10,0,-t/2])
        cylinder(d=3.5, h=t);
    translate([10,0,-t/2])
        cylinder(d=3.5, h=t);
}