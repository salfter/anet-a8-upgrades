// dimensions from PCB layout at https://github.com/adafruit/Adafruit-MAX31865-PCB

$fn=30;
translate([0,0,8.6])
%import("Adafruit MAX31865.stl");

difference()
{
    union()
    {
        cube([25.4,27.94,4]); 
        for (x=[2.54:20.32:22.86])
            translate([x, 25.4, 0])
                cylinder(d=6, h=7);
    }
    translate([12.7,17.94,0])
        cylinder(d=5.4, h=4);
    for (x=[2.54:20.32:22.86])
        translate([x, 25.4, 0])
            cylinder(d=3.1, h=7);
}

