$fn=45;

mockup();
%mount();
fan_clip();

module mockup()
{
    //%import("../alternate electronics mount/1_RAMPS_Arduino_Mount.stl");
    translate([53.3,10.6,-7])
    rotate([0,0,90])
        %import("../alternate electronics mount/1_RAMPS_Arduino_Mount_Spacer.stl");

    // translate([-40,-10,-7])
    //     %cube([100,100,.1]);

    translate([-225,483,9])
    rotate([0,0,270])
        %import("BTT SKR V1.4.stl");

    translate([50,0,20])
    rotate([0,0,90])
        %import("stepstick.stl");

    translate([0,21.59,0])
    translate([50,0,20])
    rotate([0,0,90])
        %import("stepstick.stl");

    translate([0,-21.59,0])
    translate([50,0,20])
    rotate([0,0,90])
        %import("stepstick.stl");

    translate([0,-43.18,0])
    translate([50,0,20])
    rotate([0,0,90])
        %import("stepstick.stl");

    translate([-30,40,44])
    rotate([90,0,0])
        %import("Fan_60x60x15.stl");
}

module mount()
{
    // standoffs
    translate([3.2,-12,0])
    difference()
    {
        cylinder(d=9, h=8);
        translate([0,0,-1])
        cylinder(d=3, h=10);
    }

    translate([-76.4,0,0])
    translate([3.2,-12,0])
    difference()
    {
        cylinder(d=9, h=8);
        translate([0,0,-1])
        cylinder(d=3, h=10);
    }

    translate([-0.2,102,0])
    translate([3.2,-12,0])
    difference()
    {
        cylinder(d=9, h=8);
        translate([0,0,-1])
        cylinder(d=3, h=10);
    }

    translate([-76.4,102,0])
    translate([3.2,-12,0])
    difference()
    {
        cylinder(d=9, h=8);
        translate([0,0,-1])
        cylinder(d=3, h=10);
    }

    translate([-77.7,-16.5,0])
    difference()
    {
        // main plate
        cube([131,111,3]);
        translate([10,10,-1])
            cube([66,91,5]);
        translate([86,10,-1])
            cube([25,91,5]);

        // frame mounting holes
        translate([121,37,-1])
            cylinder(d=4.5, h=5);
        translate([121,78,-1])
            cylinder(d=4.5, h=5);

        // standoff holes
        translate([77.7,16.5,-1])
        {
            translate([3.2,-12,0])
                cylinder(d=3, h=10);
            translate([-76.4,0,0])
            translate([3.2,-12,0])
                cylinder(d=3, h=10);
            translate([-0.2,102,0])
            translate([3.2,-12,0])
                cylinder(d=3, h=10);
            translate([-76.4,102,0])
            translate([3.2,-12,0])
                cylinder(d=3, h=10);

        }
    }
}

module fan_clip()
{
    translate([-26,95,-2.25])
    {
        cube([24,3,39]);
        translate([0,-10,0])
            cube([24,12,2]);
        translate([0,-10,5.5])
            cube([24,12,2]);
        translate([0,-85.5,36])
        {
            difference()
            {
                cube([24,88.5,3]);
                translate([-6,30,-1])
                    cylinder(d=56, h=5);
                translate([18.8,5.5,-1])
                    cylinder(d=3.5, h=20);
                translate([18.8,55.5,-1])
                    cylinder(d=3.5, h=20);
            }
        }
    }

}