$fn=30;

difference()
{
    union()
    {
        difference()
        {
            cube([49,49,2]);
            translate([24.5,24.5,0])
            {
                // 3 holes
                translate([-12,0,-.5])
                    cylinder(d=4.5, h=3);
                translate([12,7.5,-.5])
                    cylinder(d=4.5, h=3);
                translate([12,-7.5,-.5])
                    cylinder(d=4.5, h=3);

                // 4 holes
                // translate([-12,9,-.5])
                //     cylinder(d=4.5, h=3);
                // translate([-12,-9,-.5])
                //     cylinder(d=4.5, h=3);
                // translate([12,9,-.5])
                //     cylinder(d=4.5, h=3);
                // translate([12,-9,-.5])
                //     cylinder(d=4.5, h=3);
            }
        }

        translate([-24,-5,-18.28])
        difference()
        {

                    rotate([0,0,180])
                    translate([23.5,-16,0])
                        import("16_AM8_Right_Side_Y_Chain_Link.stl");

                    translate([0,-22,0])
                    cube([30,20,20]);

        }

        translate([-24,-10,-18])
        {
            cube([5,3,20]);
            cube([18,3,3]);
        }

        translate([-6.5,-10,-18])
            cube([5,3,20]);

        translate([-24,-10,-3])
            cube([24,22,5]);
    }

    // knock out hole for bushing mount
    // translate([24,24.5,-9.5])
    // translate([-16,-14.5,9.5])
    //     cube([32,29,2]);

}

