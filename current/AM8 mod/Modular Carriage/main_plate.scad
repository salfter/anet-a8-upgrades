//%import("../../previous/X-axis mounts/Anet_A8_X_Axis_Mount_Right.stl");

// gap under lower rod when homed (bed-to-bottom): 31 mm
// spacing between X-axis rods (center-to-center): 46 mm
// stock carriage width: 60 mm

use <LM8UU_housing_3_point_anet_a8.scad>
use <GT2_tooth.scad>

// render these one at a time to print, or all at once for preview

hotend_clamp();
%hotend_plate();
%carriage_plate();
%tensioner();

//bltouch_spacer();

// for a 1.4 mm belt and PLA, 1.6 mm seems to be a good starting value
// adjust the "loosen" variable below for other materials or belt thicknesses

module belt_retainer()
{
    loosen=0.15; // increase for a thicker belt or to loosen the fit
    for (i=[0:2:18])
    translate([i+1,1.6+loosen-.75,0])
        gt2_tooth(7);
    translate([0,-4,0])
        cube([20,4,7]);
    translate([0,1.6+loosen,0])
        cube([20,4-loosen,7]);
}

// -------------------------------------------------------------
// rods (not to be rendered)

//translate([4.6,0,7.3])
translate([0,100,35])
rotate([90,0,0])
    %cylinder(d=8, $fn=90, h=200);

translate([0,0,46]) // spacing between X-axis rods
//translate([4.6,0,7.3])
translate([0,100,35])
rotate([90,0,0])
    %cylinder(d=8, $fn=90, h=200);

// -------------------------------------------------------------
// current carriage (not to be rendered)

// translate([11.5,-31,16.5])
// rotate([90,0,90])
// %import("../../current/AM8 mod/e3d-v6-carriage-mods/1_E3DV6_BasePlate_V4_fixed.stl");

// -------------------------------------------------------------
// hotend model (not to be rendered)

translate([0,0,-2.88]) // place on bed
translate([38.2,-3.3,46.15])
//translate([29.7,-5,46.15])
//rotate([0,0,-90])
    %import("../../../previous/fan_duct_3/E3D_v6_To_Spec.stl");

// translate([33.1,0,0])
//     cylinder(d=0.4, h=0.5, $fn=20);

// hotend cooler (not to be rendered)

translate([33,0,18.75])
rotate([90,0,90])
{
    %import("../../E3D-v6_Fan_Duct/V6.6_Duct.stl");
    translate([-22,15,0])
        %import("../../E3D-v6_Fan_Duct/Fan 30mm.stl");
}

// -------------------------------------------------------------
// bed-level sensor model (not to be rendered)

translate([0,0,-1.4])
translate([-16,0,45.05])
rotate([0,0,-90])
    %import("BLTouch_Model.stl");

// -------------------------------------------------------------
// A8-compatible fan duct (not to be rendered)

// translate([60,0,1]) // this one's a non-standard height
// rotate([0,0,-90])
//     %import("../../previous/_Spriya__-_High_Efficiency_Anet_A8_Fan_Duct_/files/Spriya_-_Anet_A8_Fan_Duct.stl");

// translate([31.5,23,1.5])
// rotate([0,0,-90])
//     %import("../../previous/Semi-Circular_Anet_A8_Fan_Duct/Semi-Circular_Anet_A8_Fan_Duct.stl");

// translate([58.5,0,1.5])
// rotate([0,0,-60])
//     %import("../../Spriya_Anet_A8_Fan_Duct_Modified_for_shorter_nozzle_height/Spriya_-_Anet_A8_Fan_Duct_mod.stl");

// translate([32.5,0,1])
// rotate([90,0,-90])
//     %import("../../previous/Anet_A8_Fan_Duct_WTF_edition/Anet_A8_Fan_Duct_WTF_edition.stl");

// -------------------------------------------------------------
// 50mm blower (not to be rendered)

translate([0,0,-3.5])
translate([55.75,-7.75,25])
rotate([90,0,90])
    %import("../../../previous/fan_duct_3/50mm_Fan.stl");

// -------------------------------------------------------------
// belt tensioner

module tensioner()
{
    gap=0.25; // adjust for proper fit (try 0.25 for PLA, 0.35 for PETG)

    translate([0,0,-3.2])
    difference()
    {
        // -------------------------------------------------------------
        // main body

        union()
        {
            translate([-3.5,-30,55])
            rotate([90,0,90])
                belt_retainer();

            translate([3.5,-30,51])
                cube([7.85,20,9.6]);
        }

        // -------------------------------------------------------------
        // cut out notch for rail

        translate([11.5,-30,55.5])
        rotate([90,0,180])
        linear_extrude(20, convexity=10)
            polygon([[0,-1.5-gap],[1.2,-2-gap],[1.2,2+gap],[0,1.8+gap]]);

        // -------------------------------------------------------------
        // drill screw hole

        translate([7,35,55.5])
        rotate([90,0,0])
            cylinder(d=3.5, h=70, $fn=30);
        translate([7,-14,55.5])
        rotate([0,30,0])
        rotate([90,0,0])
            cylinder(d=6.8, h=16, $fn=6);
    }
}

// -------------------------------------------------------------
// E3D-compatible groovemount half

module groovemount()
{
    // groovemount dimensions:
    // larger diameter: 16 mm
    // smaller diameter: 12 mm, 6 mm high, set 3.7 mm down from top
    // total height: 12.3 mm

    difference()
    {
        union()
        {
            difference()
            {
                cylinder(d=20, h=12.3, $fn=90);
                cylinder(d=16, h=12.3, $fn=90);
            }
            translate([0,0,2.6])
            difference()
            {
                cylinder(d=16, h=6, $fn=90);
                cylinder(d=12, h=6, $fn=90);
            }
        }
        translate([0,-11,-.1])
            cube([22,22,13]);
    }
}

// -------------------------------------------------------------
// hotend mounting clamp

module hotend_clamp()
{
    adj=1; // tweak duct length downward (protip: 2mm was too long)

    difference()
    {
        union()
        {
            translate([33.07,0,49.1])
            rotate([0,0,180])
                groovemount();
            translate([33.07,-22.5,49.1])
            difference()
            {
                cube([23,45,12.3]);
                translate([0,22.5,0])
                    cylinder(d=18, h=12.3, $fn=90);
            }


            // -------------------------------------------------------------
            // blower mount

            translate([52.07,-8,25])
            {
                cube([4,10,35]);
                translate([0,0,-2])
                    cube([4,47,10]);
            }
            translate([52.07,-4,67])
            rotate([90,41.5,90])
            difference()
            {
                linear_extrude(4, convexity=10)
                {
                    circle(d=10, $fn=60);
                    translate([59,0])
                        circle(d=10, $fn=60);
                    translate([0,-5])
                    square([59,10]);
                }
                for (x=[1:57:58])
                translate([x,0,0])
                {
                    cylinder(d=3, h=4, $fn=30);
                    cylinder(d=6.8, h=2, $fn=6);
                }
            }

            // -------------------------------------------------------------
            // integrated blower duct

            // blower inlet
            difference()
            {
                translate([55.07,-9.75,20-adj])
                    cube([17,23,10+adj]);
                translate([56.07,-8.75,20-adj])
                    cube([15,21,10+adj]);
                translate([55.07,-14.5,35])
                rotate([-25,0,0])
                    cube([17.2,32,10]);
            }

            // spout
            translate([0,0,-adj])
            difference()
            {
                translate([42,-9.75,2])
                rotate([0,-15,0])
                union()
                {
                    difference()
                    {
                        cube([31,23,5]);
                        translate([0,1,1])
                            cube([30,21,3]);
                        translate([56.07,-8.5,10])
                            cube([15.2,20,10]);
                    }
                    translate([0,7,0])
                        cube([16.1,1,5]);;
                    translate([0,15,0])
                        cube([16.1,1,5]);;
                }
                translate([56.07,-8.75,11])
                    cube([15.2,21,10]);
                translate([56.07,-8.75,10])
                    cube([5,21,10]);
            }

            // connector
            translate([70.6,-9.75,10.1-adj])
                cube([1.5,23,10+adj]);;
            translate([54.9,-9.75,10-adj])
                cube([17.2,1.5,10+adj]);;
            translate([54.9,11.75,10-adj])
                cube([17.2,1.5,10+adj]);;
            translate([51.975,-9.75,10-adj])
                cube([4.1,23,14+adj]);;

        }

        for (y=[-15:30:15])
        translate([0,y,55.25])
        rotate([0,90,0])
        {
            cylinder(d=4.5, h=80, $fn=30);
            translate([0,0,42])
                cylinder(d=8.1, h=39, $fn=60);
        }
        translate([52,39.2,28.5])
        rotate([-10,0,0])
        rotate([0,90,0])
        {
            cylinder(d=6.8, h=2.1, $fn=6);
            cylinder(d=3.4, h=4.5, $fn=30);
        }
    }
    

}

// -------------------------------------------------------------
// hotend plate

module hotend_plate()
{
    difference()
    {
        union()
        {
            // -------------------------------------------------------------
            // groovemount    

            translate([33.07,0,49.1])
                groovemount();

            // -------------------------------------------------------------
            // groovemount standoff

            translate([16.07,-22.5,49.1])
            difference()
            {
                cube([17,45,12.3]);
                translate([17,22.5,0])
                    cylinder(d=18, h=12.3, $fn=90);
            }

            // -------------------------------------------------------------
            // main plate    

            difference()
            {
                translate([14.5,-30,19])
                    cube([3,60,78]);

                for (x=[-25:50:25])
                for (y=[24:68:92])
                translate([-20,x,y])
                rotate([0,90,0])
                        cylinder(d=3.1, $fn=30, h=40);
            }
        }

        // -------------------------------------------------------------
        // hotend clamp holes    

        for (y=[-15:30:15])
        translate([0,y,55.25])
        rotate([0,90,0])
        {
            cylinder(d=4.5, h=50, $fn=30);
            cylinder(d=8.05, h=28, $fn=6);
        }

        // -------------------------------------------------------------
        // zip-tie mounting point for hotend cables

        translate([13.5,3,92])
        {
            cube([12,5,3]);
            cube([3,5,12]);
        }
    }
}

// -------------------------------------------------------------
// BLTouch spacer

module bltouch_spacer()
{
    t=1.4;
    difference()
    {
        union()
        {
            translate([-20.5,-7.5,45.1])
                cube([9,15,t]);
            translate([-16,-7.5,45.1])
                cylinder(d=9, h=t, $fn=60);
            translate([-16,7.5,45.1])
                cylinder(d=9, h=t, $fn=60);

        }
        for (y=[-9:18:9])
        translate([-16,y,35])
            cylinder(d=3.4, h=20, $fn=30);
        translate([-16,0,35])
            cylinder(d=6, h=20, $fn=60);
        for (y=[-9:18:9])
        translate([-16,y-1.7,45.1])
            cube([5,3.4,t]);
    }
}

// -------------------------------------------------------------
// carriage plate, with belt tensioner, BLTouch mount, and bearing blocks

module carriage_plate()
{
    difference()
    {
        union()
        {
            // -------------------------------------------------------------
            // bearing holders

            translate([0,-15.5,81])
            rotate([90,0,0])
                top(4, 15.3, "no", "no", "no", "no");

            translate([0,15.5,81])
            rotate([-90,0,0])
                top(4, 15.3, "no", "no", "no", "no");    


            translate([0,0,35])
            rotate([-90,0,0])
                top(4, 15.3, "no", "no", "no", "no");

            // -------------------------------------------------------------
            // main plate    

           translate([11.5,-30,19])
               cube([3,60,78]);

            // -------------------------------------------------------------
            // right-side belt retainer

            translate([0,0,-3.2])
            {
                translate([-3.5,10,55])
                rotate([90,0,90])
                    belt_retainer();

                translate([3.5,10,51])
                    cube([8,20,9.6]);
            }

            // -------------------------------------------------------------
            // lower mounting hole surrounds

            for (x=[-25:50:25])
            translate([7.5,x,24])
            {
                rotate([0,90,0])
                    cylinder(d=10, $fn=90, h=4);
            }

            // -------------------------------------------------------------
            // BLTouch mount

            translate([0,0,-1.4])
            {
                translate([-20.5,-7.5,45.1])
                    cube([25,15,3]);
                translate([-16,-7.5,45.1])
                    cylinder(d=9, h=3, $fn=60);
                translate([-16,7.5,45.1])
                    cylinder(d=9, h=3, $fn=60);
            }
            
            translate([-8.5,4.5,45.1])
            rotate([-90,0,0])
            linear_extrude(3, convexity=10)
                polygon([[0,0],[0,8],[8,0]]);
            translate([-8.5,-7.5,45.1])
            rotate([-90,0,0])
            linear_extrude(3, convexity=10)
                polygon([[0,0],[0,8],[8,0]]);            
        }

        // -------------------------------------------------------------
        // plate mounting holes

        for (x=[-25:50:25])
        for (y=[24:68:92])
        translate([-20,x,y])
        {
            rotate([0,90,0])
                cylinder(d=3.4, $fn=30, h=40);
            rotate([30,0,0])
            rotate([0,90,0])
                cylinder(d=6.8, $fn=6, h=31.5);
        }

        // -------------------------------------------------------------
        // BLTouch mounting holes

        translate([0,0,-1.4])
        {
            for (y=[-9:18:9])
            translate([-16,y,35])
                cylinder(d=3.1, h=20, $fn=30);
            translate([-16,0,35])
                cylinder(d=6, h=20, $fn=60);
        }

        // -------------------------------------------------------------
        // tensioner holes

        translate([0,0,-3.2])
        {
            translate([7,35,55.5])
            rotate([90,0,0])
                cylinder(d=3.5, h=70, $fn=30);
            translate([7,30,55.5])
            rotate([90,0,0])
                cylinder(d=6, h=16, $fn=60);
        }

        // -------------------------------------------------------------
        // tensioner clearance

        translate([0,0,-3.2])
        translate([1.5,-30,50])
            cube([10,40,1+3.2]);

        // -------------------------------------------------------------
        // keep bridging from intruding into bearing holders

        translate([-8.45,-35,79.5])
            cube([1,70,3]);
        translate([-8.45,-35,33.5])
            cube([1,70,3]);

        // -------------------------------------------------------------
        // zip-tie mounting point for BLTouch cables

        translate([-.5,3,93])
        {
            cube([12,5,3]);
            translate([9,0,0])
            cube([3,5,12]);
        }
    
        // -------------------------------------------------------------
        // clear out BLTouch mount intrusion into lower bushing mount

        translate([0,100,35])
        rotate([90,0,0])
            cylinder(d=15.3, $fn=90, h=200);
    }

    // -------------------------------------------------------------
    // track for left-side belt retainer

    translate([0,0,-3.2])
    {
        translate([11.5,-30,55.5])
        rotate([90,0,180])
        linear_extrude(40, convexity=10)
            polygon([[0,-1.5],[1,-2],[1,2],[0,1.5]]);
    }
}

