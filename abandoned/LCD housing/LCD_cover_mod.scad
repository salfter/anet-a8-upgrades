rotate([180,0,0])
{
	difference()
	{
		union()
		{
			difference()
			{
				translate([0,0,-12])
					import("LCD_cover.stl");
					
				translate([0,0,-20])
				linear_extrude(20, convexity=10)
					polygon([[0,0],[0,100],[110,100],[110,0]]);
			}

			translate([76,0,0])
			rotate([-90,0,0])
			linear_extrude(3, convexity=10)
			{
				polygon([[-10,0],[-10,15],[-5,20],[15,20],[20,15],[20,0]]);
				translate([-5,15])
					circle(5, $fn=20);
				translate([15,15])
					circle(5, $fn=20);
			}
		}

		translate([81,4,-14.7])
		rotate([90,0,0])
		linear_extrude(5, convexity=10)
		circle(1.7, $fn=20);
	}
}
