                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


https://www.thingiverse.com/thing:2786292
Anet Evolution by dbrandao is licensed under the Creative Commons - Attribution license.
http://creativecommons.org/licenses/by/3.0/

# Summary

Anet Evolution (En)
===========
This project is to transform a Anet A8 Stock in a Core XY machine with minimum material.

We took the Amazing Scott's project on the Hypercube Evolution and customized the parts to make it the most cost effective as possible and keeping the good print quality of the Hypercube Evolution.

Guidelines
--------------
* We want to use minimum material and keep the most stock parts as possible. 
* We will use 2020 extrusion instead of 3030 on the Original Hypercube Evolution project
* The number before the name of the file, is how many copies you need to print of each part.

Material
-----------
- Hardware
  * Frame
    6x 380mm 2020 Extrusion
    4x 420mm 2020 Extrusion
    4x 500mm 2020 Extrusion
    20x Anchor Connector Universal PG20 *
  * Bed
    2x 405mm
    2x 140mm
    4x Anchor Connector Universal PG20 *
    
    
    > ###Optional
    >You can exchange the 500mm 2020 extrusion for 500mm 3030R extrusion and close the Anet Evolution with Acrilic or Polycarbonate. and enclosure on itself.

    * You can change the **Anchor Connector** with 20 L brackets and 20 corner connections for 2020, but the Anchor Connector is just perfect for the job and cheaper
- **Fasteners**
  38 x M5x10 Button head screws ([Aliexpress Sugestion][10])
  18 x M3 Socket head screws ([Aliexpress Sugestion][11])
  38 x M5 T Hammer Nuts for 2020 extrusion ([Aliexpress Sugestion][12])
  14 x M3 5x5 Brass knurled insert ([Banggood Sugestion][14])
  Alan Ofuji made an awesome job creating new parts that uses less screws. Please, take a look [https://www.thingiverse.com/thing:2852228][15]
- **Pulleys**
  6 x GT2 Idler Pulley (20 teeth) 3mm bore ([Aliexpress Sugestion][21])([Aliexpress Sugestion 2][23])
  2 x GT2 Idler Pulley without teeth (20 teeth) 3mm bore ([Aliexpress Sugestion][22])([Aliexpress Sugestion 2][23])
- **Belts**
  5m GT2 6mm belt rubber with fiber glass ([Aliexpress Sugestion][30])
- **Wire**
  3 meter Wire AWG22 2 pin.

Ps.: Se você for Brasileiro, eu adquiri os perfis e conectores com o Rafael Leiroz, da [Enilegho][2]. Recomendo muitíssimo o trabalho deles.

Electronics
---------------
- I recomend this [box][50] for the electronics. But you can use anyone made for the AM8 project. My favorite is also on the files.

Firmware
------------
For this project, you will need to get on [Marlin][40].
  - Install [Arduino IDE][41]
  - Install https://github.com/SkyNet3D/anet-board
  - Download https://github.com/MarlinFirmware/Marlin
  - Copy **Marlin/example_configurations/Anet/A8/Configuration.h** to **Marlin/Configuration.h**
  - On the **Configuration.h** from this git to **Marlin/Configuration.h** you must change:
  > #### Configuration.h
  > \#define STRING_CONFIG_H_AUTHOR "(Bob Kuhn/Thingiverse, Anet Evolution)"
  > \#define COREXY
  > \#define INVERT_Y_DIR true
  > \#define INVERT_Z_DIR false
  > \#define X_MIN_POS -17 // You must adjust this as your mounting must be different than mine.
  > \#define Y_MIN_POS 224 // You must adjust this as your mounting must be different than mine.
  > \#define Z_MIN_POS 0
  - Copy **Marlin/example_configurations/Anet/A8/Configuration_adv.h** to **Marlin/Configuration_adv.h**
  - Open **Marlin.ino** with **Arduino IDE**, choose **Anet 1.0 board** and corresponding comm port.
  - Press **Upload**

Changelog
--------------
2018-03-28 - Corrected the numbers of screws due to the new parts.
2018-03-22 - The files 1_XY_Motor_Left_3030R.stl and 1_XY_Motor_Right_3030R.stl were added to the project. Those are only for the optional 3030R extrusion and self enclosure.
2018-03-05 - The file 1_Carriage_X.stl was ajusted to the Anet A8 Metal Carriage. The Dowel pins are no longer necessary
2018-02-27 - The file 4_Bed_Bracket.stl was ajusted. the new version is on files.

Anet Evolution (Pt-Br)
===========
Este projeto é para transformar a Anet A8 original em uma CoreXY usando o mínimo de material.

Nós pegamos o Projeto do Scott, Hypercube Evolution, e customizamos algumas partes  para ter o custo mais barato, mantendo a qualidade do projeto original.

Ponto de Partida
--------------
* Usaremos o mínimo de material extra, aproveitando o máximo possível da ANET A8, pra ficar o mais barato possível
* Usaremos o perfil 2020 ao invés do 3030 original, também pela questão do custo.
* O número na frente de cada arquivo refere-se à quantidade de cópias necessárias de cada peça.

Material
-----------
- Ferragens
  * Frame
    6x 380mm Perfil 2020
    4x 420mm Perfil 2020 
    4x 500mm Perfil 2020 
    20x Conector Universal Interno
  * Mesa
    2x 405mm
    2x 140mm
    4x Conector Universal Interno
    
    
    > ###Opcional
    >Você pode trocar o perfil vertical 2020 de 500mm por um 3030R de 500mm e Policarbonato/Acrílico e ja fazer o fechamento da impressora nela mesma. Fica bem bacana.
    * Voce pode trocar o conector universal interno por 20 conexoes L e 20 cantoneiras de alumínio, mas eu não recomendo. Na prática o conector fica mais bonito, mais firme e mais barato.

- Parafusos
  38 x Parafusos M5 cabeça de botão ([Aliexpress][10])
  18 x Parafusos M3 cabeça alta ([Aliexpress][11])
  38 x Porca T M5 pra perfil 2020 ([Aliexpress][12])
  14 x Insert de Bronze M3 5x5 ([Banggood][14])
  Alan Ofuji Fez um trabalho maravilhoso com um remix de partes usando menos parafusos. Deem uma olhada [https://www.thingiverse.com/thing:2852228][15]
- Polias
  6 x GT2 Polia dentada com roldana (20 dentes), furo de 3mm ([Aliexpress][21])([Aliexpress 2][23])
  2 x GT2 Polia sem dentes com roldana (20 dentes) furo de 3mm ([Aliexpress][22])([Aliexpress 2][23])
- Correia
  5m Correia GT2 6mm de borracha com alma de fibra de vidro ([Aliexpress Sugestion][30])
- Fios
  3m Fio 0.5mm duplo, preferencia pra preto e vermelho.

Ps.: Se você for Brasileiro, eu adquiri os perfis e conectores com o Rafael Leiroz, da [Enilegho][2]. Recomendo muitíssimo o trabalho deles.

Eletrônica
---------------
- Eu recomendo esta [caixa][50] para a eletrônica. Mas você pode usar qualquer uma do projeto AM8 que lhe agrade. Minha favorita está nos arquivos.

Firmware
------------
Para esse projeto, você vai precisar do firmware [Marlin][40].
  - Instale [Arduino IDE][41]
  - Instale https://github.com/SkyNet3D/anet-board
  - Baixe o https://github.com/MarlinFirmware/Marlin
  - Copie o **Marlin/example_configurations/Anet/A8/Configuration.h** para Marlin/Configuration.h
  - No **Configuration.h** que está em Marlin/Configuration.h você deve alterar os seguintes parâmentros: 
  > #### Configuration.h
  > \#define STRING_CONFIG_H_AUTHOR "(Bob Kuhn/Thingiverse, Anet Evolution)"
  > \#define COREXY
  > \#define INVERT_Y_DIR true
  > \#define INVERT_Z_DIR false
  > \#define X_MIN_POS  -17// Ajuste aqui pra sua distância, que pode ser diferente da minha
  > \#define Y_MIN_POS 224// Ajuste aqui pra sua distância, que pode ser diferente da minha
  > \#define Z_MIN_POS 0
  - Copie o **Marlin/example_configurations/Anet/A8/Configuration_adv.h** para **Marlin/Configuration_adv.h**
  - Abra o arquivo **Marlin.ino** com o programa **Arduino IDE**, escolha a placa **Anet 1.0** e a porta COM correspondente.
  - Pressione **Upload**

Controle de Mudanças
--------------
2018-03-28 - Corrigidos os totais de parafusos para corresponder com as peças novas.
2018-03-22 - Os arquivos 1_XY_Motor_Left_3030R.stl e 1_XY_Motor_Right_3030R.stl foram adicionados. Eles servem apenas para quem for usar o perfil 3030R (abaulado) para o fechamento da impressora.
2018-03-05 - O Arquivo 1_Carriage_X.stl foi ajustado pro carro original da Anet. Os pinos de aço nao sao mais necessários
2018-02-27 - O arquivo 4_Bed_Bracket.stl foi ajustado para melhor acomodar a mesa. O novo arquivo está nos downloads.

[2]:https://www.facebook.com/guga.leroiz "Perfis e Conectores"
[10]: https://www.aliexpress.com/item/100pcs-lot-M5-10-Bolt-A2-70-ISO7380-Button-Head-Socket-Screw-Bolt-SUS304-Stainless-Steel/32328885247.html "M5 Button head screws"
[11]: https://www.aliexpress.com/item/Hex-Socket-Head-Cap-Screw-M3-Qty-90pcs-in-Box-Assortment-Kits-SUS-304-M3-4/32334431524.html "M3 Socket head screws"
[12]: https://www.aliexpress.com/item/M5-T-Nut-Hammer-Nut-Aluminum-Connector-T-Fastener-Sliding-Nut-Nickel-Plated-Carbon-Steel-for/32619352982.html "M5 T Hammer Nuts for 2020 extrusion"
[13]: https://www.aliexpress.com/item/GB119-304-Stainless-Steel-Cylindrical-Pin-Locating-Pin-M3-12/32789184323.html "3mm dowel pins"
[14]: http://www.banggood.com/100pcs-M3x5x5mm-Metric-Threaded-Brass-Knurl-Round-Insert-Nuts-p-1050182.html "M3 5x5 Brass knurled insert"
[15]: https://www.thingiverse.com/thing:2852228 "Remixed Parts"
[20]: https://www.aliexpress.com/item/2GT-pulley-H-type-wheel-driven-wheel-perlin-idler-wheel-GT2-timing-pulley-20-teeth-bore/32692297078.html "GT2 Timing Pulley (20 teeth) 5mm bore for 6mm belt"
[21]: https://www.aliexpress.com/item/2GT-pulley-H-type-wheel-driven-wheel-perlin-idler-wheel-GT2-timing-pulley-20-teeth-bore/32692297078.html "GT2 Idler Pulley (20 teeth) 3mm bore"
[22]: https://www.aliexpress.com/item/2GT-pulley-H-type-wheel-driven-wheel-perlin-idler-wheel-GT2-timing-pulley-20-teeth-bore/32692297078.html "GT2 Idler Pulley without teeth (20 teeth) 3mm bore"
[23]: https://www.aliexpress.com/item/CHANGTA-GT2-Pulley-16-20-Without-Teeth-Pulley-16-20Teeth-OR-without-Teeth-Timing-Gear-Bore/32820391885.html?spm=a2g0s.9042311.0.0.5G5q2d "Idler Pulley"
[30]: https://www.aliexpress.com/item/1pc-5m-GT2-Timing-Belt-6mm-Width-Fiber-Reinforced-Rubber-Timing-Belt-For-CNC-3D-Printer/32790795728.html "GT2 6mm belt rubber with fiber glass"
[40]: http://marlinfw.org/ "Marlin Firmware"
[41]: https://www.arduino.cc/en/Main/Software "Arduino IDE"
[50]: https://www.thingiverse.com/thing:2330654 "AM8 Electronics"

# Print Settings

Printer: ANET A8
Rafts: Doesn't Matter
Supports: Doesn't Matter

Notes: 
I would like to thank [Osvali Junior][1] and [Rafael "Guga" Leiroz][2] for the support on this project.

[1]:https://www.thingiverse.com/osvalijr/about "Osvali Junior"
[2]:https://www.facebook.com/guga.leroiz "Perfis e Conectores"