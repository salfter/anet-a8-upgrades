difference()
{
    import("SCS8UU_ADIv4.stl");

    translate([17,11,-1])
       cylinder(d=15, h=32, $fn=30);
}

translate([17,11,0])
{
    id=11;
    id_tol=.3;
    od=18;
    od_tol=0;
    len=30;
    chamfer=.4;

    rotate_extrude(convexity=2, $fn=90)
        polygon([[(id+id_tol)/2+chamfer,0],
                [(id+id_tol)/2,chamfer],
                [(id+id_tol)/2,len-chamfer],
                [(id+id_tol)/2+chamfer,len],
                [(od-od_tol)/2-chamfer,len],
                [(od-od_tol)/2,len-chamfer],
                [(od-od_tol)/2,chamfer],
                [(od-od_tol)/2-chamfer,0]]);
}
