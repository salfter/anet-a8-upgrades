id=11;
id_tol=.3;
od=15;
od_tol=.3;
len=24;
chamfer=.4;

rotate_extrude(convexity=2, $fn=90)
	polygon([[(id+id_tol)/2+chamfer,0],
			 [(id+id_tol)/2,chamfer],
			 [(id+id_tol)/2,len-chamfer],
			 [(id+id_tol)/2+chamfer,len],
			 [(od-od_tol)/2-chamfer,len],
			 [(od-od_tol)/2,len-chamfer],
			 [(od-od_tol)/2,chamfer],
			 [(od-od_tol)/2-chamfer,0]]);
