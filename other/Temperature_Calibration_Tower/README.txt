                   .:                     :,                                          
,:::::::: ::`      :::                   :::                                          
,:::::::: ::`      :::                   :::                                          
.,,:::,,, ::`.:,   ... .. .:,     .:. ..`... ..`   ..   .:,    .. ::  .::,     .:,`   
   ,::    :::::::  ::, :::::::  `:::::::.,:: :::  ::: .::::::  ::::: ::::::  .::::::  
   ,::    :::::::: ::, :::::::: ::::::::.,:: :::  ::: :::,:::, ::::: ::::::, :::::::: 
   ,::    :::  ::: ::, :::  :::`::.  :::.,::  ::,`::`:::   ::: :::  `::,`   :::   ::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  :::::: ::::::::: ::`   :::::: ::::::::: 
   ,::    ::.  ::: ::, ::`  :::.::    ::.,::  .::::: ::::::::: ::`    ::::::::::::::: 
   ,::    ::.  ::: ::, ::`  ::: ::: `:::.,::   ::::  :::`  ,,, ::`  .::  :::.::.  ,,, 
   ,::    ::.  ::: ::, ::`  ::: ::::::::.,::   ::::   :::::::` ::`   ::::::: :::::::. 
   ,::    ::.  ::: ::, ::`  :::  :::::::`,::    ::.    :::::`  ::`   ::::::   :::::.  
                                ::,  ,::                               ``             
                                ::::::::                                              
                                 ::::::                                               
                                  `,,`


http://www.thingiverse.com/thing:1478258
Temperature Calibration Tower by m4RT1n is licensed under the Creative Commons - Attribution - Share Alike license.
http://creativecommons.org/licenses/by-sa/3.0/

# Summary

a simple tower to test the quality of different temperatures.
print it with 1 perimeter, no infill / top layers (vase mode).
you can use the scale on the right to time the temperature change 
or input it in the slicer.

in normal mode with 0,2mm layers change the temperature all 50 layers
- <a href="http://www.banggood.com/Wholesale-3D-Printer-and-Supplies-c-3533.html?p=7R141814955672015049">Bangood - 3D Printer & Supplies</a> 
<br>
happy printing :)

if you like it please FOLLOW me or/and give it a LIKE :) , thx

# Print Settings

Printer Brand: RepRap
Printer: Tarantula
Rafts: No
Supports: No
Resolution: 0,2 mm
Infill: no

# Post-Printing

![Alt text](https://cdn.thingiverse.com/assets/f0/6b/33/d1/40/20160409_164929.jpg)

![Alt text](https://cdn.thingiverse.com/assets/46/d2/32/22/5d/20160409_165034.jpg)